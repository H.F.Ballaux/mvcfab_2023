<?php

namespace app\controllers;

use app\helpers\output;
use app\helpers\tools;

abstract class controller
{
    use tools;
    public function list(): void
    {
        $class = $this->getClassName();
        $model = new $class();
        $data = new \stdClass();
        $data->data = $model->getAll();
        output::getContent($class.'/'.$class.'list', $data);
    }
}