<?php

namespace app\controllers;
use app\helpers\output;
use app\models\course;

class courses extends controller
{
    public function list(): void
    {
        $class = $this->getClassName();
        $model = new course();
        $data = new \stdClass();
        $data->data = $model->getAll();
        output::getContent($class.'/'.$class.'list', $data);
    }
}