<?php

namespace app\controllers;

use app\helpers\database;
use app\models\migration;
use stdClass;

class migrations
{
    public static function exec_csv($filename): string
    {
        if (($source = fopen($filename, 'r')) !== false) {
            $table = explode('.', $filename)[0];
            $fields = database::getFields($table);
            fgetcsv($source); // on saute la premiere ligne qui contient les noms des champs
            $msg = 'Migration ' . PHP_EOL;
            $datas = [];
            while ($data = fgetcsv($source)) {
                $element = new stdClass();
                for ($i = 0; $i < sizeof($fields); $i++) {
                    $field = $fields[$i];
                    $element->$field = $data[$i];
                }
                $datas[] = $element;
            }
            $msg .= migration::execute_migrations($datas, $table);
            fclose($source);
            return $msg;
        } else {
            return 'Lecture du fichier: ' . $filename . ' impossible';
        }
    }
}