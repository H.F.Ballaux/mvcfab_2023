<?php

namespace app\controllers;

use app\helpers\output;
use app\models\user;

class users
{
    public function list(): void
    {
        $model = new user();
        $data = new \stdClass();
        $data->data = $model->getAll();
        output::getContent('users/usersList', $data);
    }

    public function getuser(int $user): void
    {
        $model = new user();
        $data = new \stdClass();
        $data->data = $model->getUser($user);
        unset($data->data->pwd);
        output::getContent('users/profile', $data);
    }
}