<?php

namespace app\helpers;

use PDO;
use PDOException;

class database
{
    public static function connect()
    {
        global $connect;

        if (!is_a($connect, 'PDO')) {
            try {
                $connect = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8', DB_USER, DB_PASSWORD);
            } catch (PDOException $exception) {
                die ('Erreur: ' . $exception->getMessage());
            }
        }
        return $connect;
    }

    public static function getTables(): array
    {
        $connect = database::connect();
        $tables = [];
        $sql = 'Show tables';
        $query = $connect->prepare($sql);
        $attr = 'Tables_in_' . DB_NAME;
        $query->execute();
        while ($table = $query->fetchObject()) {
            $tables[] = $table->$attr;
        }
        return $tables;
    }

    public static function getFields(string $table): array
    {
        if (in_array($table, self::getTables())) {
            $connect = database::connect();
            $fields = [];
            $sql = 'describe ' . $table;
            $query = $connect->prepare($sql);
            $query->execute();
            while ($field = $query->fetchObject()) {
                $fields[] = $field->Field;
            }
            return $fields;

        }
        return ['Cette table n\'existe pas'];
    }
}