<?php

namespace app\helpers;

trait tools
{
    public static function getClassName(): string
    {
        $class = get_called_class();
        $class = explode('\\', $class);
        return strtolower(end($class));
    }
}