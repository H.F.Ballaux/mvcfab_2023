<?php

namespace app\models;

use app\helpers\database;
use PDOException;

abstract class migration
{
    public static function execute_migrations(array $datas, string $table): string
    {
        $connect = database::connect();
        if (in_array($table, database::getTables())) {
            try {
                $connect->beginTransaction();
                $fields = implode(', ', database::getFields($table));
                foreach ($datas as $data) {
                    $fields_values = [];
                    $params = [];

                    foreach ($data as $key => $value) {
                        $fields_values[] = '?';
                        $params[] = $value;
                    }

                    $fields_values = implode(', ', $fields_values);
                    $query = $connect->prepare('Insert into ' . $table . ' (' . $fields . ') values (' . $fields_values . ')');
                    if (!$query->execute($params)) {
                        $connect->rollBack();
                        return 'L\'insertion a échouée';
                    }
                }
                $connect->commit();
                return 'Insertion réussie de ' . sizeof($datas) . ' entrées dans la table ' . $table . ' de la base ' . DB_NAME;
            } catch (PDOException $e) {
                $connect->rollBack();
                return 'Erreur PDO : ' . $e->getMessage();
            }
        } else {
            return 'La table: ' . $table . ' n\'existe pas! ';
        }
    }
}