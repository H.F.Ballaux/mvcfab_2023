<?php

namespace app\models;

use app\helpers\database;
use app\helpers\tools;

abstract class model
{
    use tools;
    public function getAll(): array
    {
        $datas = [];
        $class = $this->getClassName();
        $connect = database::connect();
        $request = $connect->prepare('SELECT * FROM '.$class);
        $request->execute();
        while ($course = $request->fetchObject()) {
            $datas[] = $course;
        }
        return $datas;
    }
}