<?php

namespace app\models;
use app\helpers\database;
class user extends model
{
    public function getUser(int $user){
        $class = $this->getClassName();
        $connect = database::connect();
        $sql = 'select * from '.$class.' where id = ?';
        $param = [$user];
        $query = $connect->prepare($sql);
        $query->execute($param);
        $result = $query->fetchObject();
        unset($result->pwd);
        return $result;
    }
}