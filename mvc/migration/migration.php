<?php
require_once '../config.php';
require_once '../app/controllers/migrations.php';
require_once '../app/models/migration.php';
require_once '../app/helpers/database.php';


if (php_sapi_name() !== 'cli') {
    echo 'Attention: ce script doit etre utilisé depuis votre terminal! ';
    exit(1);
}

if ($argc == 2){
    if ($argv[1] == '-all'){
        $tables = \app\helpers\database::getTables();
        rsort($tables);
        foreach ($tables as $table){
            $filename = $table.'.csv';
            if (file_exists($filename)){
                echo \app\controllers\migrations::exec_csv($filename).PHP_EOL;
            }
        }
    } else {
        $filename = $argv[1];
        if (explode('.', $filename)[1] != 'csv'){
            echo 'Mauvais format de fichier, assurez vous que ce soit bien du .CSV';
        }else{
            echo \app\controllers\migrations::exec_csv($filename);
        }
    }
} else {
    echo 'Usage: php test.php <nom_du_fichier>  | to get ce targeted file'.PHP_EOL.'Or : php test.php -all to get all the files with the same name as a DB_table ';
    exit(1);
}
