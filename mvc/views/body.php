<main role="main" class="container-fluid">
    <?php

    use app\helpers\routing;

    // Vérifie la présence d'un paramètre view dans l'URL
    if (!empty($_GET['view']) && $_GET['view'] != 'view/body') {
        // Routing
        routing::routing($_GET['view']);
    }
    ?>
</main>