<?php
if (!is_object($data)){
    return false;
}

?>

<table>
    <thead>
    <tr>
        <th>intitule</th>
        <th>valeur</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data->data as $key=>$value){
        if (in_array($key, ['password', 'id', 'image'])){
            continue;
        } else {
            if ($key == 'admin') {
                $value = ($value) ? 'oui': 'non';
            }
            echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
        }
    }
    ?>
    </tbody>
</table>
