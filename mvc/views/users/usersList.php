<?php
if (!is_object($data)) {
    return false;
}
?>

<table>
    <thead>
    <tr>
        <th>nom</th>
        <th>code</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data->data as $user) {
        echo '
            <tr>
                <td><a href="index.php?view=api/users/getuser/'.$user->id.'">' . $user->username . '</a></td>
                <td>' . $user->email . '</td>
            </tr>
            ';
    }
    ?>
    </tbody>
</table>
